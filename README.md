ISTRUZIONI:
	INSTALLAZIONE

		Per eseguire il programma è necessario aver installato java sul proprio computer, 
		il programma è stato sviluppato e testato con jre1.8.0_191 e jdk1.8.0_191.

		Per far si che il programma funzioni è necessario aver un database al quale si 
		possa appoggiare. 

		Una volta installato MySql 8 ed aver avviato il processo di MySql, creare un 
		database MYSQL chiamato “assignment3” con username “root” e password ”root”.

		Per eseguire il programma si può lanciare il JAR eseguibile “assignment3_808227”.

		Per muoversi all’interno del programma basta inserire il numero corrispondente alla 
		voce del menu che si desidera.

	UTILIZZO

		Ogni persona ha un utente. Per creare un utente selezionare la 2 voce del menu, 
		una volta fatto ciò si possono creare i propri personaggi gruppi e campagne.

		Per creare un personaggio, effettuare il login ed entrare nel menù dei personaggi, 
		in seguito tramite la 2 voce seguire il processo guidato di creazione.

		Per aggiungere una Campagna dopo aver effettuato il login si può usare la 2 voce del 
		secondo menu.

		Una volta creata una campagna è possibile dal menu del personaggio aggiungere dei 
		personaggi alla campagna.

		Per creare un gruppo serve prima aver creato una campagna, una volta fatto ciò si può tornare 
		al menu principale, entrare nel menu dei gruppi e tramite la 2 voce scegliere il nome del gruppo
		 e la campagna a cui partecipa.

		Una volta creato il gruppo gli altri giocatori possono unirsi. 
		Solo il creatore può eliminare il gruppo. 
