package assignment3;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Scanner;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Persistence;

import assignment3.Person;
import javax.persistence.ManyToMany;

@Entity
public class Campaign {
	private @Id @GeneratedValue(strategy = GenerationType.IDENTITY) int idCampaign;
	@Column(name = "name")
	private String name;
	@ManyToOne
	private Player_group player_group;
	private String gameVersion;
	@OneToMany(mappedBy = "campaign")
	private Collection<PC> pC;
	@ManyToMany
	@JoinTable(name = "campaign_person", joinColumns = @JoinColumn(name = "person_IDPERSON"), inverseJoinColumns = @JoinColumn(name = "campaign_IDCAMPAIGN"))
	private Collection<Person> person;

	private static Scanner reader = new Scanner(System.in);
	private static EntityManagerFactory entityManagerFactory;

	public Campaign() {
		this.person = new ArrayList<Person>();
		this.pC = new ArrayList<PC>();
	}

	public Campaign(String name, String gameVersion) {
		this.name = name;
		this.gameVersion = gameVersion;
		this.person = new ArrayList<Person>();
		this.pC = new ArrayList<PC>();

	}

	public int getIdCampaign() {
		return idCampaign;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Nome Campagna: " + this.name + "; Versione gioco: " + this.gameVersion;
	}

	public Player_group getPlayer_group() {
		return player_group;
	}

	public boolean setPlayer_group(Player_group param) {
		if (this.player_group == null) {
			this.player_group = param;
			return true;
		} else
			return false;
	}

	public String getGameVersion() {
		return gameVersion;
	}

	public void setGameVersion(String param) {
		this.gameVersion = param;
	}

	public Collection<PC> getPC() {
		return pC;
	}

	public void setPC(Collection<PC> param) {
		this.pC = param;
	}

	public boolean addPC(PC param) {
		if (!this.pC.contains(param)) {
			this.pC.add(param);
			return true;
		} else
			return false;
	}

	public Collection<Person> getPerson() {
		return person;
	}

	public void setPerson(Collection<Person> param) {
		this.person = param;
	}

	public boolean addPerson(Person param) {
		if (!this.person.contains(param)) {
			this.person.add(param);
			return true;
		}
		return false;
	}

	public static boolean add_Campaign(Person p) {
		entityManagerFactory = Persistence.createEntityManagerFactory("assignment3");
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();
		String name;
		do {
			System.out.println("Inserisci nome:");
			name = reader.nextLine();
		} while (name.equals(""));
		String gameVersion;
		do {
			System.out.println("Inserisci Versione del gioco utilizzata:");
			gameVersion = reader.nextLine();
		} while (gameVersion.equals(""));
		Campaign campaign = new Campaign(name, gameVersion);

		Person person = entityManager.find(Person.class, p.getIdPerson());
		person.addCampaign(campaign);
		campaign.addPerson(person);
		// System.out.println(campaign);
		entityTransaction.begin();
		entityManager.persist(person);
		entityManager.persist(campaign);
		entityTransaction.commit();
		entityManager.close();
		return true;
	}

	public static boolean delete_Campaign(Person p) {
		reader = new Scanner(System.in);
		entityManagerFactory = Persistence.createEntityManagerFactory("assignment3");
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		Collection<Campaign> resultList = entityManager.createQuery("SELECT pc FROM Campaign pc", Campaign.class)
				.getResultList();
		int c = 0;
		boolean continua = false;
		if (resultList.size() != 0 && resultList != null) {
			System.out.println("----------------------------------------");
			for (Campaign e : resultList) {
				for (Person x : e.getPerson())
					if (x.getIdPerson() == p.getIdPerson()) {
						c++;
						continua = true;
						System.out.println(c + ": " + e);
					}
			}

			System.out.println("----------------------------------------");
		} else {
			System.out.println("Non hai ancora creato nessuna Campagna");
			entityManager.close();
			return false;
		}
		if (continua == true) {
			System.out.println("Inserire il numero della Campagna che vuoi eliminare:");

			int i = reader.nextInt();
			if (i <= c && i > 0) {
				Campaign campaign = new Campaign();
				i = i - 1;
				ArrayList<Campaign> list = new ArrayList<Campaign>(resultList);
				campaign = list.get(i);
				entityManager.getTransaction().begin();
				Collection<PC> pcs = entityManager.createQuery("SELECT pc FROM PC pc", PC.class).getResultList();
				for (PC e : pcs) {
					if (e.getCampaign() != null && e.getCampaign().getIdCampaign() == campaign.getIdCampaign()) {
						entityManager.remove(e);
					}
				}
				entityManager.remove(campaign);
				entityManager.getTransaction().commit();
				System.out.println("Campagna eliminata");
			} else {
				System.out.println("Valore errato");
				entityManager.close();
				return false;
			}
		}
		entityManager.close();
		return true;
	}

	public static boolean all_Campaign(Person p) {
		entityManagerFactory = Persistence.createEntityManagerFactory("assignment3");
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		Collection<Campaign> resultList = entityManager
				.createQuery("SELECT campaign FROM Campaign campaign", Campaign.class).getResultList();
		if (resultList.size() != 0 && resultList != null) {
			System.out.println("----------------------------------------");

			for (Campaign e : resultList) {
				for (Person x : e.getPerson())
					if (x.getIdPerson() == p.getIdPerson()) {
						System.out.println(e);
					}
			}

			System.out.println("----------------------------------------");
		} else {
			System.out.println("Non hai ancora creato nessuna Campagna");
			entityManager.close();
			return false;
		}
		entityManager.close();
		return true;

	}

	public static boolean update_Campaign(Person p) {

		reader = new Scanner(System.in);
		entityManagerFactory = Persistence.createEntityManagerFactory("assignment3");
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();
		Collection<Campaign> resultList = entityManager.createQuery("SELECT pc FROM Campaign pc", Campaign.class)
				.getResultList();
		int c = 0;
		boolean continua = false;
		if (resultList.size() != 0 && resultList != null) {
			System.out.println("----------------------------------------");
			for (Campaign e : resultList) {
				for (Person x : e.getPerson())
					if (x.getIdPerson() == p.getIdPerson()) {
						c++;
						continua = true;
						System.out.println(c + ": " + e);
					}
			}

			System.out.println("----------------------------------------");
		} else {
			System.out.println("Non hai ancora creato nessuna Campagna");
			entityManager.close();
			return false;
		}
		if (continua == true) {
			System.out.println("Inserire il numero della Campagna che vuoi modificare:");
			int i = reader.nextInt();
			if (i <= c && i > 0) {
				Campaign pc = new Campaign();
				i = i - 1;
				ArrayList<Campaign> list = new ArrayList<Campaign>(resultList);
				pc = list.get(i);
				do {
					System.out.println("Cambia nome:");
					pc.setName(reader.nextLine());
				} while (pc.getName().equals(""));
				do {
					System.out.println("Cambia versione gioco:");
					pc.setGameVersion(reader.nextLine());
				} while (pc.getGameVersion().equals(""));
				entityTransaction.begin();
				entityManager.persist(pc);
				entityTransaction.commit();
				entityManager.close();
				return true;
			} else {
				System.out.println("Valore errato");
				entityManager.close();
				return false;
			}
		} else {
			System.out.println("Non hai ancora creato nessun PG");
			entityManager.close();
			return false;
		}

	}

	public static boolean add_campaign_to_player_group(Person p) {
		reader = new Scanner(System.in);
		entityManagerFactory = Persistence.createEntityManagerFactory("assignment3");
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		Collection<Player_group> resultList = entityManager
				.createQuery("SELECT player_group FROM Player_group player_group", Player_group.class).getResultList();
		int c = 0;
		boolean continua = false;
		if (resultList.size() != 0 && resultList != null) {
			System.out.println("----------------------------------------");
			for (Player_group e : resultList) {
				if (e.getMaster().getIdPerson() == p.getIdPerson()) {
					c++;
					continua = true;
					System.out.println(c + ": " + e);
				}
			}

			System.out.println("----------------------------------------");
		} else {
			System.out.println("Non hai ancora creato nessun Gruppo");
			entityManager.close();
			return false;
		}
		if (continua == true) {
			System.out.println("Inserire il numero del Gruppo a cui vuoi aggiungere una campagna:");
			int i = reader.nextInt();
			if (i <= c && i > 0) {
				Player_group pc = new Player_group();
				i = i - 1;
				ArrayList<Player_group> list = new ArrayList<Player_group>(resultList);
				pc = list.get(i);

				Collection<Campaign> campaigns = entityManager
						.createQuery("SELECT campaigns FROM Campaign campaigns", Campaign.class).getResultList();
				c = 0;
				continua = false;
				if (resultList.size() != 0 && campaigns != null) {
					System.out.println("----------------------------------------");
					for (Campaign e : campaigns) {
						for (Person x : e.getPerson())
							if (x.getIdPerson() == p.getIdPerson()) {
								c++;
								continua = true;
								System.out.println(c + ": " + e);
							}
					}

					System.out.println("----------------------------------------");
				} else {
					System.out.println("Non hai ancora creato nessuna Campagna");
					entityManager.close();
					return false;
				}
				if (continua == true) {
					System.out.println("Inserire il numero della Campagna che vuoi aggiungere:");
					i = reader.nextInt();
					if (i <= c && i > 0) {
						Campaign campaign = new Campaign();
						i = i - 1;
						ArrayList<Campaign> list2 = new ArrayList<Campaign>(campaigns);
						campaign = list2.get(i);
						if (campaign.getPlayer_group() == null) {
							campaign.setPlayer_group(pc);
							entityManager.getTransaction().begin();
							entityManager.persist(pc);
							entityManager.getTransaction().commit();
							entityManager.close();
							System.out.println("Campagna aggiunta al gruppo");
							return true;
						} else {
							System.out.println("Questa campagna appartiene gi� ad un gruppo");
							return false;
						}
					} else {
						System.out.println("Valore errato");
						entityManager.close();
						return false;
					}
				} else {
					System.out.println("Non hai ancora creato nessuna campagna da aggiungere ad un gruppo");
					entityManager.close();
					return false;
				}
			} else {
				System.out.println("Valore errato");
				entityManager.close();
				return false;
			}
		} else {
			System.out.println("Non hai ancora creato nessun gruppo");
			entityManager.close();
			return false;
		}
	}
}
