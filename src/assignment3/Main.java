package assignment3;

import java.util.Scanner;

public class Main {

	public static Scanner reader = new Scanner(System.in);

	private static void player_menu(Person p) {
		String scelta = "5";
		do {
			System.out.println("Benvenuto " + p.getNickname() + ", cosa desideri fare? ");
			System.out.println("1- Gestisci PG");
			System.out.println("2- Gestisci Campagne");
			System.out.println("3- Gestisci Gruppi");
			System.out.println("4- ELIMINA QUESTO UTENTE");
			System.out.println("5- logout");
			reader = new Scanner(System.in);
			scelta = reader.next();
			switch (scelta) {
			case "1":
				PG_menu(p);
				break;
			case "2":
				Campaign_menu(p);
				break;
			case "3":
				player_group_menu(p);
				break;
			case "4":
				boolean res = Person.delete_Person(p);
				if (res == true) {
					scelta = "5";
					System.out.println("UTENTE ELIMINATO, TORNO AL MENU");
				} else {
					System.out.println("Errore nell'eliminazione utente");
				}
				break;
			default:
				break;
			}
		} while (!scelta.equals("5"));
	}

	private static boolean player_group_menu(Person p) {

		String scelta = "6";
		do {
			System.out.println("MENU Gruppi:");
			System.out.println("1- Elenco Gruppi");
			System.out.println("2- Crea Gruppo");
			System.out.println("3- Modifica Gruppo");
			System.out.println("4- Elimina Gruppo");
			System.out.println("5- Unisciti ad un Gruppo");
			System.out.println("6- indietro");
			reader = new Scanner(System.in);
			scelta = reader.next();
			switch (scelta) {
			case "1":
				Player_group.all_player_group(p);
				break;
			case "2":
				Player_group.add_player_group(p);
				break;
			case "3":
				Player_group.update_player_group(p);
				break;
			case "4":
				Player_group.delete_player_group(p);
				break;
			case "5":
				Player_group.add_person_to_player_group(p);
				break;
			case "6":
				System.out.println("Torno al menu del giocatore...");
				break;
			default:
				break;
			}
		} while (!scelta.equals("6"));
		return true;
	}

	private static void Campaign_menu(Person p) {
		String scelta = "6";
		do {
			System.out.println("MENU Campagne:");
			System.out.println("1- Elenco Campagne");
			System.out.println("2- Aggiungi Campagna");
			System.out.println("3- Modifica Campagna");
			System.out.println("4- Aggiungi Campagna ad un gruppo");
			System.out.println("5- Elimina Campagna");
			System.out.println("6- indietro");
			reader = new Scanner(System.in);
			scelta = reader.next();
			switch (scelta) {
			case "1":
				Campaign.all_Campaign(p);
				break;
			case "2":
				Campaign.add_Campaign(p);
				break;
			case "3":
				Campaign.update_Campaign(p);
				break;
			case "4":
				Campaign.add_campaign_to_player_group(p);
				break;
			case "5":
				Campaign.delete_Campaign(p);
				break;
			case "6":
				System.out.println("Torno al menu del giocatore...");
				break;
			default:
				break;
			}
		} while (!scelta.equals("6"));
	}

	private static void PG_menu(Person p) {
		String scelta = "6";
		do {
			System.out.println("MENU PG:");
			System.out.println("1- Elenco PG");
			System.out.println("2- Aggiungi PG");
			System.out.println("3- Modifica PG");
			System.out.println("4- Elimina PG");
			System.out.println("5- Aggiungi PG ad una Campagna");
			System.out.println("6- indietro");
			reader = new Scanner(System.in);
			scelta = reader.next();
			switch (scelta) {
			case "1":
				PC.all_PG(p);
				break;
			case "2":
				PC.add_PG(p);
				break;
			case "3":
				PC.update_PG(p);
				break;
			case "4":
				PC.delete_PG(p);
				break;
			case "5":
				PC.add_pg_to_Campaign(p);
				break;
			case "6":
				System.out.println("Torno al menu del giocatore...");
				break;
			default:
				break;
			}
		} while (!scelta.equals("6"));
	}

	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		boolean res;
		String scelta = "3";
		do {

			System.out.println("Scegliere l'operazione desiderata:");
			System.out.println("1- login");
			System.out.println("2- registrazione");
			System.out.println("3- esci");
			scelta = reader.next();
			switch (scelta) {
			case "1":
				System.out.println("INIZIO LOGIN:");
				System.out.println("Inserisci nome utente:");
				String usr = null;
				do {
					usr = reader.nextLine();
				} while (usr.equals(""));
				System.out.println("Inserisci password:");

				String pwd_lgn = null;
				do {
					pwd_lgn = reader.nextLine();
				} while (pwd_lgn.equals(""));

				Person p = new Person();
				p = Person.login(usr, pwd_lgn);
				if (p != null) {
					System.out.println("Accesso effettuato!");
					player_menu(p);
				} else {
					System.out.println("Nome utente o password errati");
				}
				break;
			case "2":
				String pwd, pwd2;
				System.out.println("CREAZIONE NUOVO UTENTE:");
				String usr_name;
				do {
					System.out.println("Inserisci il tuo nome:");
					usr_name = reader.nextLine();
				} while (usr_name.equals(""));

				String usr_surname;
				do {
					System.out.println("Inserisci il tuo cognome:");
					usr_surname = reader.nextLine();
				} while (usr_surname.equals(""));
				String usr_nickname;
				do {
					System.out.println("Inserisci il tuo nome utente:");
					usr_nickname = reader.nextLine();
				} while (usr_nickname.equals(""));
				String city;
				do {
					System.out.println("Inserisci citt� di provenienza:");
					city = reader.nextLine();
				} while (city.equals(""));
				do {
					System.out.println("Inserisci password:");
					pwd = reader.next();
					System.out.println("Ripetere password:");
					pwd2 = reader.next();
					if (pwd.equals(pwd2)) {
						res = Person.add_Person(usr_name, usr_surname, city, usr_nickname, pwd);
						if (res == true) {
							System.out.println("Utente aggiunto,effettuare il login per continuare.");
						} else {
							System.out.println("Utente non aggiunto.");
						}
					} else {
						System.out.println("Le password non coincidono, riprovare.");
					}
				} while (!pwd.equals(pwd2));
				break;
			case "3":
				System.out.println("Uscita effettuata.");
			default:
				break;
			}
		} while (!scelta.equals("3"));
		reader.close();
	}
}