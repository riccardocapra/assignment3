package assignment3;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Scanner;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Persistence;

import assignment3.Campaign;

@Entity
public class PC {
	private @Id @GeneratedValue(strategy = GenerationType.IDENTITY) Long idPG;
	@Column(name = "name")
	private String name;
	@Column(name = "race")
	private String race;
	@ManyToOne
	private Person person;
	@Column(name = "pg_class")
	private String pg_class;
	@ManyToOne
	@JoinColumn(name = "CAMPAIGN_IDCAMPAIGN")
	private Campaign campaign;

	private static Scanner reader = new Scanner(System.in);
	private static EntityManagerFactory entityManagerFactory;

	public PC() {

	}

	public PC(String name, String race, String classs, Person p) {
		this.name = name;
		this.race = race;
		this.pg_class = classs;
		this.person = p;
	}

	@Override
	public String toString() {
		return "nome: " + this.name + "; razza: " + this.race + "; classe: " + this.pg_class;
	}

	public Long getIdPG() {
		return idPG;
	}

	public void setIdPG(Long idPG) {
		this.idPG = idPG;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRace() {
		return race;
	}

	public void setRace(String race) {
		this.race = race;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person param) {
		this.person = param;
	}

	public String getPg_class() {
		return pg_class;
	}

	public void setPg_class(String param) {
		this.pg_class = param;
	}

	public Campaign getCampaign() {
		return campaign;
	}

	public boolean setCampaign(Campaign param) {
		if (this.campaign == null) {
			this.campaign = param;
			return true;
		} else {
			return false;
		}
	}

	public static boolean delete_PG(Person p) {

		reader = new Scanner(System.in);
		entityManagerFactory = Persistence.createEntityManagerFactory("assignment3");
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		Collection<PC> resultList = entityManager.createQuery("SELECT pc FROM PC pc", PC.class).getResultList();
		int c = 0;
		boolean continua = false;
		if (resultList.size() != 0) {
			System.out.println("----------------------------------------");
			for (PC e : resultList) {
				if (e.getPerson().getIdPerson() == p.getIdPerson()) {
					c++;
					continua = true;
					System.out.println(c + ": " + e);
				}
			}
			System.out.println("----------------------------------------");
		} else {
			System.out.println("Non hai ancora creato nessun PG");
			entityManager.close();
			return false;
		}
		if (continua == true) {
			System.out.println("Inserire il numero del PG che vuoi eliminare:");

			int i = reader.nextInt();
			if (i <= c && i > 0) {
				PC pc = new PC();
				i = i - 1;
				ArrayList<PC> list = new ArrayList<PC>(resultList);
				pc = list.get(i);
				entityManager.getTransaction().begin();
				entityManager.remove(pc);
				entityManager.getTransaction().commit();
				System.out.println("PG eliminato");
			} else {
				System.out.println("Valore errato");
				entityManager.close();
				return false;
			}
		}
		entityManager.close();
		return true;
	}

	public static boolean update_PG(Person p) {
		reader = new Scanner(System.in);
		entityManagerFactory = Persistence.createEntityManagerFactory("assignment3");
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();
		Collection<PC> resultList = entityManager.createQuery("SELECT pc FROM PC pc", PC.class).getResultList();
		int c = 0;
		boolean continua = false;
		if (resultList.size() != 0) {
			System.out.println("----------------------------------------");
			for (PC e : resultList) {
				if (e.getPerson().getIdPerson() == p.getIdPerson()) {
					c++;
					continua = true;
					System.out.println(c + ": " + e);
				}
			}
			System.out.println("----------------------------------------");
		} else {
			System.out.println("Non hai ancora creato nessun PG");
			entityManager.close();
			return false;
		}
		if (continua == true) {
			System.out.println("Inserire il numero del PG che vuoi modificare:");
			int i = reader.nextInt();
			if (i < c && i > 0) {
				PC pc = new PC();
				i = i - 1;
				ArrayList<PC> list = new ArrayList<PC>(resultList);
				pc = list.get(i);
				do {
					System.out.println("Cambia nome:");
					pc.setName(reader.nextLine());
				} while (pc.getName().equals(""));
				do {
					System.out.println("Cambia razza:");
					pc.setRace(reader.nextLine());
				} while (pc.getRace().equals(""));
				do {
					System.out.println("Cambia classe:");
					pc.setPg_class(reader.nextLine());
				} while (pc.getPg_class().equals(""));
				entityTransaction.begin();
				entityManager.persist(pc);
				entityTransaction.commit();
				entityManager.close();
				return true;
			} else {
				System.out.println("Valore errato");
				entityManager.close();
				return false;
			}

		} else {
			System.out.println("Non hai ancora creato nessun PG");
			entityManager.close();
			return false;
		}
	}

	public static boolean add_PG(Person p) {
		reader = new Scanner(System.in);
		entityManagerFactory = Persistence.createEntityManagerFactory("assignment3");
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();
		String name;
		do {
			System.out.println("Inserisci nome:");
			name = reader.nextLine();
		} while (name.equals(""));
		String race;
		do {
			System.out.println("Inserisci razza:");
			race = reader.nextLine();
		} while (race.equals(""));
		String classs;
		do {
			System.out.println("Inserisci classe:");
			classs = reader.nextLine();
		} while (classs.equals(""));
		PC pc = new PC(name, race, classs, p);
		entityTransaction.begin();
		entityManager.persist(pc);
		entityTransaction.commit();
		entityManager.close();
		return true;
	}

	public static boolean all_PG(Person p) {
		// System.out.println("id pg:" + p.getIdPerson());
		entityManagerFactory = Persistence.createEntityManagerFactory("assignment3");
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		Collection<PC> resultList = entityManager.createQuery("SELECT pc FROM PC pc", PC.class).getResultList();
		if (resultList.size() != 0) {
			System.out.println("----------------------------------------");
			for (PC e : resultList) {
				if (e.getPerson().getIdPerson() == p.getIdPerson())
					System.out.println(e);
			}
			System.out.println("----------------------------------------");
		} else {
			System.out.println("Non hai ancora creato nessun PG");
			entityManager.close();
			return false;
		}
		entityManager.close();
		return true;
	}

	public static boolean add_pg_to_Campaign(Person p) {
		reader = new Scanner(System.in);
		entityManagerFactory = Persistence.createEntityManagerFactory("assignment3");
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		Collection<PC> resultList = entityManager
				.createQuery("SELECT pc FROM PC pc WHERE pc.person = :person", PC.class).setParameter("person", p)
				.getResultList();
		int c = 0;
		if (resultList.size() != 0) {
			System.out.println("----------------------------------------");
			for (PC e : resultList) {
				c++;
				System.out.println(c + ": " + e);
			}
			System.out.println("----------------------------------------");
		} else {
			System.out.println("Non hai ancora creato nessun PG");
			entityManager.close();
			return false;
		}
		System.out.println("Inserire il numero del PG che vuoi associare ad una campagna:");
		int i = reader.nextInt();

		if (i <= c && i > 0) {
			PC pc = new PC();
			i = i - 1;
			ArrayList<PC> list = new ArrayList<PC>(resultList);
			pc = list.get(i);
			if (pc.getCampaign() == null) {
				c = 0;
				Collection<Campaign> campaigns = entityManager
						.createQuery("SELECT pc FROM Campaign pc ", Campaign.class).getResultList();
				if (campaigns.size() != 0 && campaigns != null) {
					System.out.println("----------------------------------------");
					for (Campaign e : campaigns) {
						c++;
						System.out.println(c + ": " + e);
					}
					System.out.println("----------------------------------------");
				} else {
					System.out.println("Non esiste nessuna Campagna");
					entityManager.close();
					return false;
				}
				System.out.println("Inserire il numero della campagna a cui associare il PG");
				i = reader.nextInt();
				if (i <= c && i > 0) {
					Campaign campaign = new Campaign();
					i = i - 1;
					ArrayList<Campaign> list2 = new ArrayList<Campaign>(campaigns);
					campaign = list2.get(i);
					pc.setCampaign(campaign);
					boolean aggiunto = campaign.addPC(pc);
					if (aggiunto == true) {
						Person person = entityManager.find(Person.class, p.getIdPerson());
						campaign.addPerson(person);
						p.addCampaign(campaign);
						entityManager.getTransaction().begin();
						entityManager.persist(person);
						entityManager.persist(pc);
						entityManager.persist(campaign);
						entityManager.getTransaction().commit();
					} else {
						System.out.println("Il PG � gi� parte di questa campagna");
						return false;
					}
				} else {
					System.out.println("Valore campagna errato");
					entityManager.close();
					return false;
				}

			} else {
				System.out.println("Questo PG appartiene gi� ad una campagna");
				entityManager.close();
				return false;
			}
		}
		System.out.println("PG aggiunto correttamente alla campagna");
		entityManager.close();
		return true;

	}
}
