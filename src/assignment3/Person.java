package assignment3;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Persistence;
import javax.persistence.Table;
import assignment3.Person;
import assignment3.Player_group;

@Entity(name = "Person")
@Table(name = "Person")
public class Person {

	private @Id @GeneratedValue(strategy = GenerationType.IDENTITY) Long idPerson;
	@Column(name = "name")
	private String name;
	@Column(name = "surname")
	private String surname;
	@Column(name = "city")
	private String city;
	@Column(name = "nickname", unique = true)
	private String nickname;
	@Column(name = "password")
	private String password;
	@OneToMany(mappedBy = "person", cascade = CascadeType.ALL)
	private Collection<PC> pC;

	@ManyToMany
	private Collection<Player_group> player_group;

	@ManyToMany(mappedBy = "person")
	private Collection<Campaign> campaign;

	@ManyToMany
	@JoinTable(name = "person_person", joinColumns = @JoinColumn(name = "person_IDPERSON"), inverseJoinColumns = @JoinColumn(name = "person_1_IDPERSON"))
	private Collection<Person> masters;
	@ManyToMany(mappedBy = "masters")
	private Collection<Person> pgs;

	@OneToMany(mappedBy = "master")
	private Collection<Player_group> gruppi_masterati;

	private static EntityManagerFactory entityManagerFactory;

	public Person() {
		this.pC = new ArrayList<PC>();
		this.pgs = new ArrayList<Person>();
		this.masters = new ArrayList<Person>();
		this.campaign = new ArrayList<Campaign>();
		this.player_group = new ArrayList<Player_group>();
		this.gruppi_masterati = new ArrayList<Player_group>();
	}

	public void addPC(PC pc) {
		this.pC.add(pc);
	}

	public void addPlayer_group(Player_group player_group) {
		this.player_group.add(player_group);
	}

	public Person(String name, String surname, String city, String nickname, String pwd) {
		this.name = name;
		this.surname = surname;
		this.city = city;
		this.nickname = nickname;
		this.password = pwd;
		this.pC = new ArrayList<PC>();
		this.pgs = new ArrayList<Person>();
		this.masters = new ArrayList<Person>();
		this.campaign = new ArrayList<Campaign>();
		this.player_group = new ArrayList<Player_group>();
		this.gruppi_masterati = new ArrayList<Player_group>();
	}

	public Long getIdPerson() {
		return idPerson;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "Person [id=" + idPerson + ", name=" + name + ", surname=" + surname + ", city=" + city + ", nickname="
				+ nickname + "]";
	}

	public Collection<PC> getPC() {
		return pC;
	}

	public void setPC(Collection<PC> param) {
		this.pC = param;
	}

	public Collection<Player_group> getPlayer_group() {
		return player_group;
	}

	public void setPlayer_group(Collection<Player_group> param) {
		this.player_group = param;
	}

	public Collection<Campaign> getCampaign() {
		return campaign;
	}

	public void setCampaign(Collection<Campaign> param) {
		this.campaign = param;
	}

	public void addCampaign(Campaign param) {
		this.campaign.add(param);
	}

	public Collection<Person> getpgs() {
		return pgs;
	}

	public Collection<PC> getpC() {
		return pC;
	}

	public void setpC(Collection<PC> pC) {
		this.pC = pC;
	}

	public Collection<Person> getMasters() {
		return masters;
	}

	public void setMasters(Collection<Person> masters) {
		this.masters = masters;
	}

	public boolean addMaster(Person Master) {
		if (!this.masters.contains(Master)) {
			this.masters.add(Master);
			return true;
		}
		return false;
	}

	public Collection<Person> getPgs() {
		return pgs;
	}

	public void setPgs(Collection<Person> pgs) {
		this.pgs = pgs;
	}

	public boolean addPgs(Person pgs) {
		if (!this.pgs.contains(pgs)) {
			this.pgs.add(pgs);
			return true;
		}
		return false;
	}

	public Collection<Player_group> getGruppi_masterati() {
		return gruppi_masterati;
	}

	public void setGruppi_masterati(Collection<Player_group> gruppi_masterati) {
		this.gruppi_masterati = gruppi_masterati;
	}

	public boolean addGruppi_masterati(Player_group gruppi_masterati) {
		if (!this.gruppi_masterati.contains(gruppi_masterati)) {
			this.gruppi_masterati.add(gruppi_masterati);
			return true;
		}
		return false;
	}

	public static boolean add_Person(String name, String surname, String city, String nickname, String pwd) {
		entityManagerFactory = Persistence.createEntityManagerFactory("assignment3");
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();
		Collection<Person> resultList = entityManager
				.createQuery("SELECT e FROM Person e WHERE e.nickname = '" + nickname + "'", Person.class)
				.getResultList();
		if (resultList.size() == 0) {
			Person new_player = new Person(name, surname, city, nickname, pwd);
			entityTransaction.begin();
			entityManager.persist(new_player);
			entityTransaction.commit();
			entityManager.close();
			return true;
		} else {
			/*
			 * for (Person e : resultList) System.out.println(e.toString());
			 */
			System.out.println("Nome utente gi� utilizzato");
			entityManager.close();
			return false;
		}

	}

	public static Person login(String usr, String pwd) {
		entityManagerFactory = Persistence.createEntityManagerFactory("assignment3");
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		Collection<Person> resultList = entityManager
				.createQuery("SELECT e FROM Person e WHERE e.nickname = '" + usr + "' AND e.password = '" + pwd + "'",
						Person.class)
				.getResultList();
		if (resultList.size() == 1) {
			/*
			 * for (Person e : resultList) System.out.println(e.toString());
			 */

			entityManager.close();
			return resultList.iterator().next();
		} else {
			entityManager.close();
			return null;
		}
	}

	public static boolean delete_Person(Person p) {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		Person person = entityManager.find(Person.class, p.getIdPerson());
		entityManager.getTransaction().begin();
		Collection<Player_group> resultList = entityManager
				.createQuery("SELECT player_group FROM Player_group player_group", Player_group.class).getResultList();
		for (Player_group e : resultList) {
			if (e.getMaster().getIdPerson() == person.getIdPerson()) {
				Collection<Campaign> campaigns = entityManager
						.createQuery("SELECT campaign FROM Campaign campaign WHERE campaign.player_group = :p_g",
								Campaign.class)
						.setParameter("p_g", e).getResultList();
				for (Campaign x : campaigns) {
					Collection<PC> pcs = entityManager.createQuery("SELECT pc FROM PC pc", PC.class).getResultList();
					for (PC pc : pcs) {
						if (pc.getCampaign() == x) {
							entityManager.remove(pc);
						}
					}
					x.setPlayer_group(null);
					entityManager.persist(x);
					entityManager.remove(x);
				}
				entityManager.remove(e);
			}
		}
		Collection<PC> pcs = entityManager.createQuery("SELECT pc FROM PC pc", PC.class).getResultList();
		for (PC e : pcs) {
			if (e.getPerson().getIdPerson() == person.getIdPerson()) {
				entityManager.remove(e);
			}
		}
		entityManager.remove(person);
		entityManager.getTransaction().commit();
		entityManager.close();
		return true;
	}
}
