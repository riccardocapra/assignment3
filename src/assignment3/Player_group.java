package assignment3;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Scanner;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Persistence;

import assignment3.Person;
import javax.persistence.ManyToOne;

@Entity
public class Player_group {

	@Id
	@GeneratedValue
	private int idGroup;
	private String name;

	@ManyToMany(mappedBy = "player_group")
	private Collection<Person> person;

	@OneToMany(mappedBy = "player_group")
	private Collection<Campaign> campaign;
	@ManyToOne
	@JoinColumn(name = "MASTER_IDPERSON")
	private Person master;

	private static Scanner reader = new Scanner(System.in);
	private static EntityManagerFactory entityManagerFactory;

	public Player_group() {
		this.person = new ArrayList<Person>();
		this.campaign = new ArrayList<Campaign>();
	}

	public Player_group(String name) {
		this.setName(name);
		this.person = new ArrayList<Person>();
		this.campaign = new ArrayList<Campaign>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Collection<Person> getPerson() {
		return person;
	}

	public void setPerson(Collection<Person> param) {
		this.person = param;
	}

	public boolean addPerson(Person param) {
		if (!this.person.contains(param)) {
			this.person.add(param);
			return true;
		}
		return false;
	}

	public Collection<Campaign> getCampaign() {
		return campaign;
	}

	public void setCampaign(Collection<Campaign> param) {
		this.campaign = param;
	}

	public boolean addCampaign(Campaign param) {
		if (!this.campaign.contains(param)) {
			this.campaign.add(param);
			return true;
		} else
			return false;
	}

	public Person getMaster() {
		return master;
	}

	public void setMaster(Person param) {
		this.master = param;
	}

	@Override
	public String toString() {
		return "nome: " + this.name + "; master: " + this.master.getNickname();
	}

	public static boolean add_person_to_player_group(Person p) {

		reader = new Scanner(System.in);
		entityManagerFactory = Persistence.createEntityManagerFactory("assignment3");
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		Collection<Player_group> resultList = entityManager
				.createQuery("SELECT player_group FROM Player_group player_group", Player_group.class).getResultList();
		int c = 0;
		if (resultList.size() != 0 && resultList != null) {
			System.out.println("----------------------------------------");
			for (Player_group e : resultList) {
				c++;
				System.out.println(c + ": " + e);
			}

			System.out.println("----------------------------------------");
		} else {
			System.out.println("Non hai ancora creato nessun Gruppo");
			entityManager.close();
			return false;
		}
		System.out.println("Inserire il numero del Gruppo in cui vuoi unirti:");
		int i = reader.nextInt();
		if (i <= c && i > 0) {
			Player_group pc = new Player_group();
			i = i - 1;
			ArrayList<Player_group> list = new ArrayList<Player_group>(resultList);
			pc = list.get(i);
			Person person = entityManager.find(Person.class, p.getIdPerson());
			if (!pc.getPerson().contains(person)) {
				person.addPlayer_group(pc);
				person.addMaster(pc.getMaster());
				Person master = entityManager.find(Person.class, pc.getMaster().getIdPerson());
				if (!pc.getPerson().contains(person)) {
					master.addPgs(person);
					entityManager.persist(master);
				}
				pc.addPerson(p);
				entityManager.getTransaction().begin();
				entityManager.persist(pc);
				entityManager.persist(person);
				entityManager.getTransaction().commit();
				System.out.println("Aggiunto correttamente al Gruppo");
				entityManager.close();
				return true;
			} else {
				System.out.println("Fai gi� parte di questo gruppo");
				entityManager.close();
				return false;
			}

		} else {
			System.out.println("Valore errato");
			entityManager.close();
			return false;
		}
	}

	public static boolean delete_player_group(Person p) {
		reader = new Scanner(System.in);
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		Collection<Player_group> resultList = entityManager
				.createQuery("SELECT player_group FROM Player_group player_group", Player_group.class).getResultList();
		int c = 0;
		boolean continua = false;
		if (resultList.size() != 0 && resultList != null) {
			System.out.println("----------------------------------------");
			for (Player_group e : resultList) {
				if (e.getMaster().getIdPerson() == p.getIdPerson()) {
					c++;
					continua = true;
					System.out.println(c + ": " + e);
				}
			}

			System.out.println("----------------------------------------");
		} else {
			System.out.println("Non hai ancora creato nessun Gruppo");
			entityManager.close();
			return false;
		}
		if (continua == true) {
			System.out.println("Inserire il numero del Gruppo che vuoi eliminare:");

			int i = reader.nextInt();
			if (i <= c && i > 0) {
				Player_group pc = new Player_group();
				i = i - 1;
				ArrayList<Player_group> list = new ArrayList<Player_group>(resultList);
				pc = list.get(i);
				Collection<Campaign> campaigns = entityManager
						.createQuery("SELECT campaign FROM Campaign campaign WHERE campaign.player_group = :p_g",
								Campaign.class)
						.setParameter("p_g", pc).getResultList();
				entityManager.getTransaction().begin();
				for (Campaign e : campaigns) {
					Collection<PC> pcs = entityManager.createQuery("SELECT pc FROM PC pc", PC.class).getResultList();
					for (PC pg : pcs) {
						if (pg.getCampaign() != null && pg.getCampaign().getIdCampaign() == e.getIdCampaign()) {
							pg.setCampaign(null);
							entityManager.persist(pg);
						}

					}
					if (e.getPlayer_group() == pc) {
						e.setPlayer_group(null);
						entityManager.remove(e);
					}
				}
				entityManager.remove(pc);
				entityManager.getTransaction().commit();
				System.out.println("Gruppo eliminato");
			} else {
				System.out.println("Valore errato");
				entityManager.close();
				return false;
			}
		}
		entityManager.close();
		return true;
	}

	public static boolean update_player_group(Person p) {

		reader = new Scanner(System.in);
		entityManagerFactory = Persistence.createEntityManagerFactory("assignment3");
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();
		Collection<Player_group> resultList = entityManager
				.createQuery("SELECT player_group FROM Player_group player_group", Player_group.class).getResultList();
		int c = 0;
		boolean continua = false;
		if (resultList.size() != 0 && resultList != null) {
			System.out.println("----------------------------------------");
			for (Player_group e : resultList) {
				if (e.getMaster().getIdPerson() == p.getIdPerson()) {
					c++;
					continua = true;
					System.out.println(c + ": " + e);
				}
			}

			System.out.println("----------------------------------------");
		} else {
			System.out.println("Non hai ancora creato nessun Gruppo");
			entityManager.close();
			return false;
		}
		if (continua == true) {
			System.out.println("Inserire il numero del Gruppo che vuoi modificare:");
			int i = reader.nextInt();
			if (i <= c && i > 0) {
				Player_group pc = new Player_group();
				i = i - 1;
				ArrayList<Player_group> list = new ArrayList<Player_group>(resultList);
				pc = list.get(i);
				do {
					System.out.println("Cambia nome:");
					pc.setName(reader.nextLine());
				} while (pc.getName().equals(""));
				entityTransaction.begin();
				entityManager.persist(pc);
				entityTransaction.commit();
				entityManager.close();
				return true;
			} else {
				System.out.println("Valore errato");
				entityManager.close();
				return false;
			}
		} else {
			System.out.println("Non hai ancora creato nessun PG da aggiungere ad un gruppo");
			entityManager.close();
			return false;
		}
	}

	public static boolean add_player_group(Person p) {
		reader = new Scanner(System.in);
		entityManagerFactory = Persistence.createEntityManagerFactory("assignment3");
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		String name = "";
		do {
			System.out.println("Nome gruppo:");
			name = reader.nextLine();
		} while (name.equals(""));

		int c = 0;
		int i = 0;
		Collection<Campaign> campaigns = entityManager.createQuery("SELECT pc FROM Campaign pc ", Campaign.class)
				.getResultList();
		if (campaigns.size() > 0 && campaigns != null) {
			System.out.println("----------------------------------------");
			for (Campaign e : campaigns) {
				c++;
				System.out.println(c + ": " + e);
			}
			System.out.println("----------------------------------------");

			System.out.println("Inserire il numero della campagna a cui associare il gruppo:");
			i = reader.nextInt();
			if (i <= c && i > 0) {
				Campaign campaign = new Campaign();
				i = i - 1;
				ArrayList<Campaign> list = new ArrayList<Campaign>(campaigns);
				campaign = list.get(i);
				if (campaign.getPlayer_group() == null) {
					Player_group p_g = new Player_group(name);
					campaign.setPlayer_group(p_g);
					Person person = entityManager.find(Person.class, p.getIdPerson());
					person.addPlayer_group(p_g);
					p_g.addPerson(person);
					p_g.setMaster(person);
					entityManager.getTransaction().begin();
					entityManager.persist(person);
					entityManager.persist(p_g);
					entityManager.getTransaction().commit();
					entityManager.close();
					return true;
				} else {
					System.out.println("La campagna appartiene gi� ad un gruppo.");
					return false;
				}

			} else {
				System.out.println("Valore errato.");
				return false;
			}
		} else {
			System.out.println("Nessuna campagna creata.");
			return false;
		}
	}

	public static boolean all_player_group(Person p) {
		reader = new Scanner(System.in);
		entityManagerFactory = Persistence.createEntityManagerFactory("assignment3");
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		Collection<Player_group> resultList = entityManager
				.createQuery("SELECT player_group FROM Player_group player_group", Player_group.class).getResultList();
		if (resultList.size() != 0 && resultList != null) {
			System.out.println("----------------------------------------");
			for (Player_group e : resultList) {
				if (e.getMaster().getIdPerson() == p.getIdPerson()) {
					System.out.println(e + " [MASTERATO]");
				} else {
					for (Person x : e.getPerson()) {
						if (x.getIdPerson() == p.getIdPerson()) {
							System.out.println(e);
						}
					}
				}
			}
			System.out.println("----------------------------------------");
		} else {
			System.out.println("Non hai ancora creato nessun Gruppo");
			entityManager.close();
			return false;
		}
		return true;

	}
}
