package assignment3.Tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import assignment3.Campaign;
import assignment3.PC;
import assignment3.Person;
import assignment3.Player_group;

public class CampaignTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testCampaignStringString() {
		Campaign c_test= new Campaign("c_test","versione");
		assertTrue("Campaign added", c_test.getName()=="c_test" &&  c_test.getGameVersion()=="versione"); 	
	}

	@Test
	public void testSetPlayer_group() {
		Campaign c_test= new Campaign("c_test","versione");
		Player_group pg = new Player_group("pg_name");
		assertTrue(c_test.setPlayer_group(pg)); 	
		Player_group pg2 = new Player_group("pg2_name");
		assertTrue(!c_test.setPlayer_group(pg2)); 	
	}

	@Test
	public void testAddPC() {
		Person test_person = new Person("name","surname","city","nickname1","pwd");
		PC test_pc = new PC("name","race","class",test_person);
		PC test_pc2 = new PC("name2","race2","class2",test_person);
		Campaign c_test= new Campaign("c_test","versione");
		assertTrue(c_test.addPC(test_pc)); 	
		assertTrue(!c_test.addPC(test_pc)); 	
		assertTrue(c_test.addPC(test_pc2)); 	
	}

}
