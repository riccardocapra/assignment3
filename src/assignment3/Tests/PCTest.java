package assignment3.Tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import assignment3.Campaign;
import assignment3.PC;
import assignment3.Person;

public class PCTest {


	@Before
	public void setUp() throws Exception {

	}

	@Test
	public void testGetCampaign() {
		Person test_person = new Person("name","surname","city","nickname1","pwd");
		PC test_pc = new PC("name","race","class",test_person);
		Campaign campaign = new Campaign("name","gameVersion");
		test_pc.setCampaign(campaign);
		assertTrue("Campaign added", test_pc.getCampaign() == campaign); 	
	}

	@Test
	public void testSetCampaign() {
		Person test_person = new Person("name","surname","city","test_campagna","pwd");
		PC test_pc = new PC("test set campagna","race","class",test_person);
		Campaign campaign = new Campaign("name","gameVersion");
		assertTrue("Campaign added", test_pc.setCampaign(campaign)); 
	}
	
	@Test
	public void testPCStringStringStringPerson() {
		Person test_person = new Person("name","surname","city","test_campagna","pwd");
		PC test_pc = new PC("test set campagna","race","class",test_person);
		assertTrue("Pc created", test_pc.getName()=="test set campagna" &&  test_pc.getRace()=="race"); 
	}

	@Test
	public void testGetPg_class() {
		Person test_person = new Person("name","surname","city","test_campagna","pwd");
		PC test_pc = new PC("test set campagna","race","class",test_person);
		assertTrue("Pc created", test_pc.getPg_class()=="class");
	}

	@Test
	public void testSetPg_class() {
		Person test_person = new Person("name","surname","city","test_campagna","pwd");
		PC test_pc = new PC("test set campagna","race",null,test_person);
		test_pc.setPg_class("class");
		assertTrue("Pc created", test_pc.getPg_class()=="class");
	}

}
