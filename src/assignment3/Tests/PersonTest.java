package assignment3.Tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import assignment3.Person;
import assignment3.Player_group;

public class PersonTest {

	@Before
	public void setUp() throws Exception {
	
	}

	@Test
	public void testAddPlayer_group() {
		Person test_person = new Person("name","surname","city","nickname1","pwd");
		Player_group pg = new Player_group("name");
		test_person.addPlayer_group(pg);
		assertTrue(test_person.getPlayer_group().contains(pg));
	}

	@Test
	public void testPersonStringStringStringStringString() {
		Person test_person = new Person("name","surname","city","nickname1","pwd");
		assertTrue("Campaign added", test_person.getName()=="name" &&  test_person.getSurname()=="surname" && test_person.getCity()=="city" && test_person.getNickname()=="nickname1" && test_person.getPassword()=="pwd"); 	
	}

	@Test
	public void testAddPgs() {
		Person test_person = new Person("name","surname","city","nickname1","pwd");
		Person test_master = new Person("name","surname","city","nickname1","pwd");
		Person test_person2 = new Person("name","surname","city","nickname1","pwd");
		test_master.addPgs(test_person);
		assertTrue(test_master.getPgs().contains(test_person));
		assertTrue(!test_master.getPgs().contains(test_person2));
		
		}

}
