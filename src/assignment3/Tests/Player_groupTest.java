package assignment3.Tests;

import static org.junit.Assert.*;


import org.junit.Before;
import org.junit.Test;

import assignment3.Campaign;
import assignment3.Person;
import assignment3.Player_group;

public class Player_groupTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testPlayer_groupString() {
		Player_group pg = new Player_group("name");
		assertTrue(pg.getName() =="name"); 	
	}

	@Test
	public void testAddPerson() {
		Person test_person = new Person("name", "surname", "city", "nickname1", "pwd");
		Player_group pg = new Player_group("name");
		pg.addPerson(test_person);
		assertTrue(pg.getPerson().iterator().next() ==test_person); 	
	}

	@Test
	public void testAddCampaign() {
		Player_group pg = new Player_group("name");
		Campaign c_test= new Campaign("c_test","versione");
		pg.addCampaign(c_test);
		assertTrue(pg.getCampaign().iterator().next()==c_test); 	
	}

	@Test
	public void testSetMaster() {
		Player_group pg = new Player_group("name");
		Person test_master = new Person("name", "surname", "city", "nickname1", "pwd");
		pg.setMaster(test_master);
		assertTrue(pg.getMaster()==test_master); 	
	}
	

}
